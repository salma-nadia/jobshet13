package com.example.jobsheet13_salmanadia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private  lateinit var  namaView: TextView
    private lateinit var kelasView: TextView
    private lateinit var nisView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        namaView = findViewById(R.id.namaView)
        namaView = findViewById(R.id.KelasView)
        namaView = findViewById(R.id.NisView)

        val extras = intent.extras

        namaView.text = extras?.getString("nama")
        namaView.text = extras?.getString("kelas")
        namaView.text = extras?.getString("nis")
    }
}